library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.ipbus.all;
use work.ipbus_reg_types.all;

use work.emp_data_types.all;
use work.emp_project_decl.all;
use work.emp_device_decl.all;
use work.emp_ttc_decl.all;
use work.emp_slink_types.all;

use work.dtc_link_maps.all;


entity emp_payload is
    port(
        --- Input Ports ---
        clk_p          : in  std_logic;
        clk40          : in  std_logic := '0';
        clk_payload    : in  std_logic_vector(2 downto 0);
        rst_payload    : in  std_logic_vector(2 downto 0);
        rst_loc        : in  std_logic_vector(N_REGION - 1 downto 0);
        clken_loc      : in  std_logic_vector(N_REGION - 1 downto 0);
        ctrs           : in  ttc_stuff_array;
        d              : in  ldata(4 * N_REGION - 1 downto 0);
        backpressure   : in  std_logic_vector(SLINK_MAX_QUADS-1 downto 0);
        --- Output Ports ---
        bc0            : out std_logic;
        gpio           : out std_logic_vector(29 downto 0);
        gpio_en        : out std_logic_vector(29 downto 0);
        q              : out ldata(4 * N_REGION - 1 downto 0);
        slink_q        : out   slink_input_data_quad_array(SLINK_MAX_QUADS-1 downto 0) := (others => SLINK_INPUT_DATA_ARRAY_NULL);
        --- IPBus Ports ---
        clk            : in  std_logic;
        rst            : in  std_logic;
        ipb_in         : in  ipb_wbus;
        ipb_out        : out ipb_rbus
    );
end emp_payload;

architecture rtl of emp_payload is
begin

    --==============================--
    -- BE Stup Formatting and Routing
    --==============================--

    --==============================--
    StubProcessorInstance: entity work.StubProcessor
    --==============================--
    port map(
        --- Input Ports ---
        clk_p    => clk_p,
        dtc_din  => d(cNumberOfFEModules - 1 downto 0),
        --- Output Ports ---
        dtc_dout => q(cNumberOfOutputLinks - 1 downto 0)
    );


    bc0     <= '0';
    gpio    <= (others => '0');
    gpio_en <= (others => '0');

end rtl;
