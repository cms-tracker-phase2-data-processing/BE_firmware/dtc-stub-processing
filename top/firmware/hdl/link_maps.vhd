library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package dtc_link_maps is
    constant cNumberOfFEModules : integer := 72;
    constant cNumberOfOutputLinks : integer := 36;

    -- type tDTCInputLinkMap is array(0 to cNumberOfFEModules - 1) of integer;
    -- constant cDTCInputLinkMap : tDTCInputLinkMap := (
    --     0,   1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11,
    --     12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
    --     24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35
    -- );

    -- type tDTCOutputLinkMap is array(0 to cNumberOfOutputLinks - 1) of integer;
    -- constant cDTCOutputLinkMap : tDTCOutputLinkMap := (40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57);
end package dtc_link_maps;
