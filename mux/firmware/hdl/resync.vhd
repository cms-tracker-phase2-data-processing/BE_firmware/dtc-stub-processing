library ieee;
use ieee.std_logic_1164.all;
use work.config.all;
use work.dtc_stubs.all;
use work.dtc_config.all;

entity dtc_mux_resync is
    generic (
        latency: natural
    );
    port (
        --- Input Ports ---
        clk         : in std_logic;
        resync_din  : in t_stubsDTC( numOverlap - 1 downto 0 );
        --- Output Ports ---
        resync_dout : out t_stubsDTC( numOverlap - 1 downto 0 )
    );
end;

architecture rtl of dtc_mux_resync is

    component dtc_resync_node
    generic (
        latency: natural
    );
    port (
        clk: in std_logic;
        node_din: in t_stubDTC;
        node_dout: out t_stubDTC
    );
    end component;

begin

    g: for k in numOverlap - 1 downto 0 generate

        signal node_din: t_stubDTC := nullStub;
        signal node_dout: t_stubDTC := nullStub;

    begin

        node_din <= resync_din( k );
        resync_dout( k ) <= node_dout;

        --==============================--
        ResyncNodeInstance : entity work.dtc_resync_node
        --==============================--
        generic map (
            latency => latency
        )
        port map (
            --- Input Ports ---
            clk       => clk,
            node_din  => node_din,
            --- Output Ports ---
            node_dout => node_dout
        );


    end generate;

end;
