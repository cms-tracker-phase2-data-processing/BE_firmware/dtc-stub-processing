library ieee;
use ieee.std_logic_1164.all;
use work.config.all;
use work.dtc_stubs.all;
use work.dtc_config.all;
use work.dtc_link_maps.all;

entity dtc_mux is
    port (
        --- Input Ports ---
        clk      : in std_logic;
        mux_din  : in t_stubsRoute(routeStubs - 1 downto 0);
        --- Output Ports ---
        mux_dout : out t_stubsDTC(cNumberOfOutputLinks - 1 downto 0)
    );
end;

architecture rtl of dtc_mux is
begin

    genMuxNodes : for k in TMPtfp - 1 downto 0 generate

        signal node_dout   : t_stubsDTC( numOverlap - 1 downto 0 ) := ( others => nullStub );

        function linkMapping( i: t_stubsRoute ) return t_stubsRoute is
            variable o: t_stubsRoute( cRouteBlocks - 1 downto 0 ) := ( others => nullStub );
        begin
            for j in cRouteBlocks - 1 downto 0 loop
                o( j ) := i( j * TMPtfp + k );
            end loop;
            return o;
        end function;

    begin
        --==============================--
        MuxNodeInstance : entity work.dtc_mux_node
        --==============================--
        port map (
            --- Input Ports ---
            clk       => clk,
            node_din  => linkMapping(mux_din),
            --- Output Ports ---
            node_dout => node_dout
        );

        --==============================--
        MuxResyncInstance : entity work.dtc_mux_resync
        --==============================--
        generic map (
            latency => k + 1
        )
        port map (
            --- Input Ports ---
            clk         => clk,
            resync_din  => node_dout,
            --- Output Ports ---
            resync_dout => mux_dout( numOverlap * ( k + 1 ) - 1 downto numOverlap * k )
        );
    end generate;

end;
