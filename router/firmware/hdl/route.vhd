library ieee;
use ieee.std_logic_1164.all;
use work.config.all;
use work.dtc_stubs.all;
use work.dtc_config.all;
use work.dtc_link_maps.all;

entity dtc_route is
    port (
        --- Input Ports ---
        clk        : in std_logic;
        route_din  : in t_stubsTransform(cNumberOfFEModules - 1 downto 0);
        --- Output Ports ---
        route_dout : out t_stubsRoute( routeStubs - 1 downto 0 )
    );
end;

architecture rtl of dtc_route is
begin

    genRouteBlocks : for k in cRouteBlocks - 1 downto 0 generate
    begin
        --==============================--
        RouteBlockInstance : entity work.dtc_route_block
        --==============================--
        port map (
            --- Input Ports ---
            clk        => clk,
            block_din  => route_din( routeNodeInputs * ( k + 1  ) - 1 downto routeNodeInputs * k ),
            --- Output Ports ---
            block_dout => route_dout( TMPtfp * ( k + 1  ) - 1 downto TMPtfp * k )
        );

    end generate;

end;
