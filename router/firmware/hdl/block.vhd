library ieee;
use ieee.std_logic_1164.all;
use work.config.all;
use work.dtc_stubs.all;
use work.dtc_config.all;

entity dtc_route_block is
    port (
        --- Input Ports ---
        clk        : in std_logic;
        block_din  : in t_stubsTransform( routeNodeInputs - 1 downto 0 );
        --- Input Ports ---
        block_dout : out t_stubsRoute( TMPtfp - 1 downto 0 )
    );
end;

architecture rtl of dtc_route_block is

    signal reset_din   : t_stubTransform                                  := nullStub;
    signal array_din   : t_stubsTransform( routeNodeInputs - 1 downto 0 ) := ( others => nullStub );
    signal array_reset : t_stubsRoute( TMPtfp - 1 downto 0 )              := ( others => nullStub );

begin

    process(clk) is -- TODO: Give process descriptive name
    begin
        if rising_edge(clk) then
            reset_din <= block_din(routeNodeInputs - 1);
        end if;
    end process;

    --==============================--
    DesyncInstance : entity work.dtc_route_desync
    --==============================--
    port map (
        --- Input Ports ---
        clk         => clk,
        desync_din  => block_din,
        --- Output Ports ---
        desync_dout => array_din
    );

    --==============================--
    ResetInstance : entity work.dtc_route_reset
    --==============================--
    port map (
        --- Input Ports ---
        clk        => clk,
        reset_din  => reset_din,
        --- Output Ports ---
        reset_dout => array_reset
    );

    --==============================--
    ArrayInstance : entity work.dtc_route_array
    --==============================--
    port map (
        --- Input Ports ---
        clk         => clk,
        array_din   => array_din,
        array_reset => array_reset,
        --- Output Ports ---
        array_dout  => block_dout
    );

end;
