library ieee;
use ieee.std_logic_1164.all;
use work.config.all;
use work.dtc_stubs.all;
use work.dtc_config.all;

entity dtc_route_array is
    port (
        --- Input Ports ---
        clk         : in std_logic;
        array_din   : in t_stubsTransform( routeNodeInputs - 1 downto 0 );
        array_reset : in t_stubsRoute( TMPtfp - 1 downto 0 );
        --- Output Ports ---
        array_dout  : out t_stubsRoute( TMPtfp - 1 downto 0 )
    );
end;

architecture rtl of dtc_route_array is

    type t_blockStubs is array( routeNodeInputs downto 0 ) of t_stubsRoute( TMPtfp - 1 downto 0 );
    signal bockStubs: t_blockStubs := ( others => ( others => nullStub ) );

begin

    bockStubs( routeNodeInputs ) <= array_reset;
    array_dout <= bockStubs( 0 );

    genRows: for k in routeNodeInputs - 1 downto 0 generate
    begin
        --==============================--
        RowInstance : entity work.dtc_route_row
        --==============================--
        port map (
            --- Input Ports ---
            clk      => clk,
            row_tin  => array_din(k),
            row_rin  => bockStubs(k + 1),
            --- Output Ports ---
            row_rout => bockStubs(k)
        );
    end generate;
end;
