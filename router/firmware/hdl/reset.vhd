library ieee;
use ieee.std_logic_1164.all;
use work.config.all;
use work.dtc_stubs.all;

entity dtc_route_reset is
    port (
        --- Input Ports ---
        clk        : in std_logic;
        reset_din  : in t_stubTransform;
        --- Output Ports ---
        reset_dout : out t_stubsRoute( tmpTFP - 1 downto 0 )
    );
end;

architecture rtl of dtc_route_reset is

    signal stub: t_stubTransform := nullStub;
    signal stubs: t_stubsTransform( tmpTFP downto 0 ) := ( others => nullStub );

begin

    process( clk ) is  -- TODO: Give process descriptive name
    begin
        if rising_edge( clk ) then
            stub <= reset_din;
            stubs( tmpTFP ) <= stub;
        end if;
    end process;

    genResetNodes : for k in tmpTFP - 1 downto 0 generate
    begin
        --==============================--
        ResetNodeInstance : entity work.dtc_route_reset_node
        --==============================--
        generic map (
            id => k
        )
        port map (
            --- Input Ports ---
            clk       => clk,
            node_tin  => stubs(k + 1),
            --- Output Ports ---
            node_tout => stubs(k),
            node_rout => reset_dout(k)
        );

    end generate;

end;
