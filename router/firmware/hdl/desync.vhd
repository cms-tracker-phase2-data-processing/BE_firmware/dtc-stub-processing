library ieee;
use ieee.std_logic_1164.all;
use work.config.all;
use work.tools.all;
use work.dtc_stubs.all;
use work.dtc_config.all;

entity dtc_route_desync is
    port (
        --- Input Ports ---
        clk         : in std_logic;
        desync_din  : in t_stubsTransform( routeNodeInputs - 1 downto 0 );
        --- Output Ports ---
        desync_dout : out t_stubsTransform( routeNodeInputs - 1 downto 0 )
    );
end;

architecture rtl of dtc_route_desync is
begin
    genDesyncNodes : for k in routeNodeInputs - 1 downto 0 generate
    begin
        --==============================--
        DesyncNodeInstance : entity work.dtc_route_desync_node
        --==============================--
        generic map (
            latency => routeNodeInputs - k
        )
        port map (
            --- Input Ports ---
            clk       => clk,
            node_din  => desync_din(k),
            --- Output Ports ---
            node_dout => desync_dout(k)
        );
    end generate;
end;
