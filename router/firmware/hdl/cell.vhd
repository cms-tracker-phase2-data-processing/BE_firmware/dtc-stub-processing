library ieee;
use ieee.std_logic_1164.all;
use work.config.all;
use work.tools.all;
use work.dtc_stubs.all;
use work.dtc_config.all;

entity dtc_route_cell is
    generic (
        bx : natural
    );
    port (
        --- Input Ports ---
        clk       : in std_logic;
        cell_tin  : in t_stubTransform;
        cell_rin  : in t_stubRoute;
        --- Output Ports ---
        cell_rout : out t_stubRoute;
        cell_tout : out t_stubTransform
    );
    attribute ram_style: string;
end;

architecture rtl of dtc_route_cell is

    constant cWidthRam : natural := numOverlap + widthR + widthPhiDTC + widthZ + widthMBin + widthMBin + widthSectorEta + widthSectorEta + widthLayer;
    type tRAM is array ( natural range <> ) of std_logic_vector( cWidthRam - 1 downto 0 );

    function bxs_init return naturals is
        variable r: naturals( tfpPackets - 1 downto 0 ) := ( others => 0 );
    begin
        for k in tfpPackets - 1 downto 0 loop
            r( k ) := k * tmpTFP + bx;
        end loop;
        return r;
    end function;
    constant bxs: naturals( tfpPackets - 1 downto 0 ) := bxs_init;

    signal tin, tout : t_stubTransform := nullStub;
    signal rin, rout : t_stubRoute     := nullStub;

    signal stub                : t_stubRoute                                    := nullStub;
    signal ram                 : tRAM( 2 ** widthCICstubs - 1 downto 0 )        := ( others => ( others => '0' ) );
    signal raddr, waddr, laddr : std_logic_vector( widthCICstubs - 1 downto 0 ) := ( others => '0' );

    attribute ram_style of ram : signal is "distributed";

    function bxCheck( sbx: std_logic_vector ) return boolean is
    begin
        for k in bxs'range loop
            if bxs( k ) = uint( sbx ) then
                return  true;
            end if;
        end loop;
        return false;
    end function;

begin

    tin       <= cell_tin;
    rin       <= cell_rin;
    cell_tout <= tout;
    cell_rout <= rout;

    stub     <= conv(ram(uint(raddr)));

    process(clk) is
    begin
        if rising_edge(clk) then

            tout <= tin;
            rout <= rin;
            ram( uint( waddr ) ) <= conv( tin );
            if rin.valid = '0' and rin.reset = '0' and raddr /= laddr then
                rout <= stub;
                rout.valid <= '1';
                raddr <= incr( raddr );
            end if;
            if rin.reset = '1' then
                raddr <= laddr;
                laddr <= waddr;
            end if;
            rout.bx <= rin.bx;

            if tin.valid = '1'then
                if bxCheck( tin.bx ) then
                    waddr <= incr( waddr );
                    if incr( waddr ) = raddr then
                        raddr <= incr( raddr );
                    end if;
                end if;
                if rin.bx = tin.bx then
                    laddr <= incr( waddr );
                end if;
            end if;

            if tin.reset = '1' and uint( tin.bx ) = 0 then
                raddr <= ( others => '0' );
                waddr <= ( others => '0' );
                laddr <= ( others => '0' );
            end if;

        end if;
    end process;

end;
