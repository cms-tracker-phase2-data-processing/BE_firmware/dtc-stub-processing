library ieee;
use ieee.std_logic_1164.all;
use work.config.all;
use work.dtc_stubs.all;
use work.dtc_config.all;

entity dtc_route_row is
    port (
        --- Input Ports ---
        clk      : in std_logic;
        row_tin  : in t_stubTransform;
        row_rin  : in t_stubsRoute( TMPtfp - 1 downto 0 );
        --- Output Ports ---
        row_rout : out t_stubsRoute( TMPtfp - 1 downto 0 )
    );
    end;

architecture rtl of dtc_route_row is

    signal stubsTransform: t_stubsTransform( TMPtfp downto 0 ) := ( others => nullStub );

begin

    stubsTransform( TMPtfp ) <= row_tin;

    genCells : for k in TMPtfp - 1 downto 0 generate
    begin
        --==============================--
        CellInstance : entity work.dtc_route_cell
        --==============================--
        generic map (
            TMPtfp - k - 1
        )
        port map (
            --- Input Ports ---
            clk       => clk,
            cell_tin  => stubsTransform(k + 1),
            cell_rin  => row_rin(k),
            --- Output Ports ---
            cell_rout => row_rout(k),
            cell_tout => stubsTransform(k)
        );
    end generate;
end;
