library ieee;
use ieee.std_logic_1164.all;
use work.config.all;
use work.tools.all;
use work.dtc_stubs.all;

entity dtc_route_reset_node is
    generic (
        id : natural
    );
    port (
        --- Input Ports ---
        clk       : in std_logic;
        node_tin  : in t_stubTransform;
        --- Output Ports ---
        node_tout : out t_stubTransform;
        node_rout : out t_stubRoute
    );
end;

architecture rtl of dtc_route_reset_node is

    function init_times return naturals is
        variable times: naturals( tfpPackets - 1 downto 0 ) := ( others => numBX );
    begin
        for k in tfpPackets - 1 downto 0 loop
            times( k ) := tmpTFP - id - 1 + k * tmpTFP;
        end loop;
        return times;
    end function;
    constant times: naturals( tfpPackets - 1 downto 0 ) := init_times;

    signal tin            : t_stubTransform                                  := nullStub;
    signal tout           : t_stubTransform                                  := nullStub;
    signal rout           : t_stubRoute                                      := ( '0', '0', stdu( numBX, widthBX ), others => ( others => '0' ) );
    signal reset          : std_logic                                        := '0';
    signal counterClks    : std_logic_vector( widthStubs - 1 downto 0 )      := stdu( numStubs, widthStubs );
    signal counterTFPtmps : std_logic_vector( widthTFPPackets - 1 downto 0 ) := ( others => '0' );
    signal sr             : t_stubsTransform( 1 + bxClks - 1 downto 0 )      := ( others => nullStub );

begin

    tin       <= node_tin;
    node_tout <= tout;
    node_rout <= rout;
    reset     <= '1' when tin.reset = '1' and uint( tin.bx( widthBX - 1 downto widthTMPfe ) ) = 0 else '0';
    tout      <= sr( sr'high );

    process(clk) is -- TODO: Give process descriptive name
    begin
        if rising_edge( clk ) then
            sr <= sr( sr'high - 1 downto 0 ) & tin;
            counterClks <= incr( counterClks );
            rout.reset <= '0';
            if uint( counterClks ) = numStubs - 1 and uint( counterTFPtmps ) < tfpPackets - 1 then
                counterClks <= ( others => '0' );
                counterTFPtmps <= incr( counterTFPtmps );
                rout.bx <= stdu( times( uint( incr( counterTFPtmps ) ) ), widthBX );
                rout.reset <= '1';
            end if;
            if reset = '1' then
                counterTFPtmps <= ( others => '0' );
                counterClks <= ( others => '0' );
                rout.bx <= stdu( times( 0 ), widthBX );
                rout.reset <= '1';
            end if;
        end if;
    end process;
end;
