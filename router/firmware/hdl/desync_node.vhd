library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.config.all;
use work.tools.all;
use work.dtc_stubs.all;
use work.dtc_config.all;

entity dtc_route_desync_node is
    generic (
        latency : natural
    );
    port (
        --- Input Ports ---
        clk       : in std_logic;
        node_din  : in t_stubTransform;
        --- Input Ports ---
        node_dout : out t_stubTransform
    );
    attribute ram_style: string;
end;

architecture rtl of dtc_route_desync_node is

    constant cWidthRam: natural := 1 + 1 + widthBX + numOverlap + widthR + widthPhiDTC + widthZ + widthMBin + widthMBin + widthSectorEta + widthSectorEta + widthLayer;
    type tRAM is array ( natural range <> ) of std_logic_vector( cWidthRam - 1 downto 0 );

    signal ram              : tRAM( 2 ** widthStubs - 1 downto 0 )        := ( others => ( others => '0' ) );
    signal waddr            : std_logic_vector( widthStubs - 1 downto 0 ) := ( others => '0' );
    signal raddr            : std_logic_vector( widthStubs - 1 downto 0 ) := ( others => '0' );
    signal regOptional, reg : std_logic_vector( cWidthRam - 1 downto 0 )   := ( others => '0' );

    attribute ram_style of ram : signal is "block";

    function lconv( t: t_stubTransform ) return std_logic_vector is
        variable s: std_logic_vector( cWidthRam - 1 downto 0 ) := ( others => '0' );
    begin
        s := t.reset & t.valid & t.bx & t.nonant & t.r & t.phi & t.z & t.mMin & t.mMax & t.etaMin & t.etaMax & t.layer;
        return s;
    end function;

    function lconv( s: std_logic_vector ) return t_stubTransform is
        variable t: t_stubTransform := nullStub;
    begin
        t.reset  := s( 1 + 1 + widthBX + numOverlap + widthR + widthPhiDTC + widthZ + widthMBin + widthMBin + widthSectorEta + widthSectorEta + widthLayer - 1 );
        t.valid  := s(     1 + widthBX + numOverlap + widthR + widthPhiDTC + widthZ + widthMBin + widthMBin + widthSectorEta + widthSectorEta + widthLayer - 1 );
        t.bx     := s(         widthBX + numOverlap + widthR + widthPhiDTC + widthZ + widthMBin + widthMBin + widthSectorEta + widthSectorEta + widthLayer - 1 downto numOverlap + widthR + widthPhiDTC + widthZ + widthMBin + widthMBin + widthSectorEta + widthSectorEta + widthLayer );
        t.nonant := s(                   numOverlap + widthR + widthPhiDTC + widthZ + widthMBin + widthMBin + widthSectorEta + widthSectorEta + widthLayer - 1 downto              widthR + widthPhiDTC + widthZ + widthMBin + widthMBin + widthSectorEta + widthSectorEta + widthLayer );
        t.r      := s(                                widthR + widthPhiDTC + widthZ + widthMBin + widthMBin + widthSectorEta + widthSectorEta + widthLayer - 1 downto                       widthPhiDTC + widthZ + widthMBin + widthMBin + widthSectorEta + widthSectorEta + widthLayer );
        t.phi    := s(                                         widthPhiDTC + widthZ + widthMBin + widthMBin + widthSectorEta + widthSectorEta + widthLayer - 1 downto                                     widthZ + widthMBin + widthMBin + widthSectorEta + widthSectorEta + widthLayer );
        t.z      := s(                                                       widthZ + widthMBin + widthMBin + widthSectorEta + widthSectorEta + widthLayer - 1 downto                                              widthMBin + widthMBin + widthSectorEta + widthSectorEta + widthLayer );
        t.mMin   := s(                                                                widthMBin + widthMBin + widthSectorEta + widthSectorEta + widthLayer - 1 downto                                                          widthMBin + widthSectorEta + widthSectorEta + widthLayer );
        t.mMax   := s(                                                                            widthMBin + widthSectorEta + widthSectorEta + widthLayer - 1 downto                                                                      widthSectorEta + widthSectorEta + widthLayer );
        t.etaMin := s(                                                                                        widthSectorEta + widthSectorEta + widthLayer - 1 downto                                                                                       widthSectorEta + widthLayer );
        t.etaMax := s(                                                                                                         widthSectorEta + widthLayer - 1 downto                                                                                                        widthLayer );
        t.layer  := s(                                                                                                                          widthLayer - 1 downto                                                                                                                 0 );
        return t;
    end function;

begin

    node_dout <= lconv(reg);
    waddr     <= std_logic_vector(unsigned(raddr) + latency);

    process(clk) is -- TODO: Give process descriptive name
    begin
        if rising_edge(clk) then

            ram(uint(waddr)) <= lconv(node_din);
            regOptional      <= ram(uint(raddr));
            reg              <= regOptional;
            raddr            <= incr(raddr);

        end if;
    end process;

end;
