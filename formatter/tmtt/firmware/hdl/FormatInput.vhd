library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.emp_data_types.all;
use work.tools.all;
use work.config.all;
use work.dtc_stubs.all;
use work.dtc_config.all;

entity dtc_formatInput_node is
    port (
        --- Input Ports ---
        clk       : in std_logic;
        node_din  : in lword;
        --- Input Ports ---
        node_dout : out t_stubFE
    );
    attribute ram_style: string;
end;

-- This is a far simplified fw block compared to the original design, as the interleaving stage,
-- where the streams of stubs from both CICs on a given link are merged, is conducted in a
-- separate block within the LinkInterface. If latency needs to be reduced in the future, it may
-- be possible to remove the clocking from this block entirely.
architecture rtl of dtc_formatInput_node is

    signal dout: t_stubFE := nullStub;

begin

    node_dout <= dout;

    process( clk ) is
    begin
        if rising_edge( clk ) then
            dout.bend  <= node_din.data(widthBend + widthCol + widthRow + widthBX - 1 downto widthCol + widthRow + widthBX);
            dout.col   <= node_din.data(            widthCol + widthRow + widthBX - 1 downto            widthRow + widthBX);
            dout.row   <= node_din.data(                       widthRow + widthBX - 1 downto                       widthBX);
            dout.bx    <= node_din.data(                                  widthBX - 1 downto                             0);
            dout.valid <= node_din.valid;
        end if;
    end process;
end;
