library ieee;
use ieee.std_logic_1164.all;
use work.emp_data_types.all;
use work.config.all;
use work.tools.all;
use work.dtc_stubs.all;
use work.dtc_config.all;

entity dtc_formatOutput_node is
port (
    --- Input Ports ---
    clk       : in std_logic;
    node_din  : in t_stubDTC;
    --- Input Ports ---
    node_dout : out lword
);
end;

architecture rtl of dtc_formatOutput_node is

    signal din     : t_stubDTC                                   := nullStub;
    signal dout    : lword                                       := ( ( others => '0' ), '0', '0', '0', '1', '0' );

    signal reset   : std_logic                                   := '0';
    signal counter : std_logic_vector( widthStubs - 1 downto 0 ) := ( others => '0' );

begin

    node_dout <= dout;
    din       <= node_din;

    process ( clk ) is
    begin
        if rising_edge( clk ) then

            dout.data <= ( others => '0' );
            if dout.valid = '1' then
                if din.valid = '1' then
                    dout.data <= conv( din );
                end if;
                counter <= incr( counter );
                if uint( counter ) = numStubs - downTime - 1 then
                    dout.valid <= '0';
                    dout.data <= ( others => '0' );
                end if;
            end if;

            reset <= din.reset;
            if reset = '1' then
                dout.valid <= '1';
                if din.valid = '1' then
                    dout.data <= conv( din );
                end if;
                counter <= ( others => '0' );
            end if;

        end if;
    end process;

end;
