library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use work.tools.all;
use work.trackerGeometry.all;

package config is

constant modulesPerDTC: natural := modulesPerDTC;
constant widthLayer: natural := widthLayer;
constant psModule: boolean := psModule;

--
-- INPUTS
--

constant widthZ             : natural := 14; -- number of bits used to represent gloabl z
constant widthR             : natural := 12; -- number of bits used to represent r w.r.t. chosenRofPhi
constant widthPhi           : natural := 14; -- number of bits used to represent phi w.r.t. phi subSector center

constant numOverlap         : natural := 2;      -- number of nononans a reconstructable track may cross
constant numSectorsPhi      : natural := 2;      -- number of further sectors in phi
constant numSectorsEta      : natural := 16;     -- number of further sectors in eta
--constant chosenRofPhi       : real    := 67.25; -- offest radius used for phi sector definitionmaxRtimesMoverBend21.
--constant chosenRofZ         : real    := 67.25;   -- offest radius used for eta sector definition
constant minPt              : real    := 3.0;
constant beamWindowZ        : real    := 15.0;   -- halve lumi region in z
constant maxEta             : real    := 2.4;
constant multiplierCot      : naturals( numSectorsEta - 1 downto 0 ) := ( 4, 4, 4, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 4, 4, 4 );

constant bField             : real := 3.81120228767395;                      -- b field in Tesla
constant speedOfLight       : real := 2.99792458;
constant trackerInnerRadius : real := 21.8;                        -- smallest radius
constant trackerOuterRadius : real := 112.7;                       -- biggest radius
constant trackerHalfLength  : real := 270.0;                       -- biggest |z|
constant barrelHalfLength   : real := 125.0;                       -- transition region between barrel and end caps


constant bxClks         : natural :=  9; -- clks per bx in DTC and TFP
constant tmpTFP         : natural := 18; -- time multiplex period of track finding processor
constant tmpFE          : natural :=  8; -- time multiplex period of front end (CIC)
constant numBX          : natural := 72; -- least common multiple of tmpTFP and tmpFE
constant widthBX        : natural := width( numBX );
constant widthTMPfe     : natural := width( tmpFE );
constant widthTMPtfp    : natural := width( tmpTFP );
constant tfpPackets     : natural := numBX / tmpTFP;
constant fePackets      : natural := numBX / tmpFE;
constant widthTFPPackets: natural := width( tfpPackets );

constant numCIC             : natural := 2;  -- number of parallel cic stubs in FE frame
constant widthRow           : natural := 11; -- number of bits used to represent cluster position in fine granular direction
constant widthRowLUT        : natural := 4;
constant widthCol           : natural := 5;  -- number of bits used to represent cluster position in rough granular direction per Module
constant widthColCIC        : natural := widthCol - 1;  -- number of bits used to represent cluster position in rough granular direction per CIC
constant widthBend          : natural := 6;  -- number of bits used to represent bend
--constant widthBendCIC       : natural := 4;  -- number of bits used to represent bend in 2S modules
constant widthBendCIC       : natural := 6;
constant baseDiffBend       : integer := -2;
constant bendRes            : real    := 1.25;

constant baseCol: real := 1.0; -- full pixel/strip length resolution
constant baseRow: real := 0.5; -- half pixel/strip pitch resolution
constant baseRegion: real := 2.0 * MATH_PI / real( numRegions );

constant widthDSPa: natural := 27;
constant widthDSPb: natural := 18;
constant widthDSPc: natural := 48;

constant numBarrelLayers: natural := 6;
constant numEncapWheels : natural := 5;

constant houghMinPt         : real    := 3.0;   -- smalles pt value in GeV we want to reconstruct
--constant houghNbinsPt       : natural := 36;    -- number of bins in pt in track finding
constant houghNbinsPt       : natural := 32;    -- number of bins in pt in track finding
constant houghNbinsPhi      : natural := 64;    -- number of bins in phiT in track finding
constant maxStubsInCell     : natural := 16;    -- max number of stubs per found track
--constant numStubs           : natural := 144;   -- max number of stubs per link and TMP
constant numStubs           : natural := 162;   -- max number of stubs per link and TMP
constant downTime           : natural := 6;     -- number of frames between two packets
constant minStubLayers      : natural := 5;     -- min number of detector layer/disc a found track must have subs in
constant numEtaReduceLayers : natural := 2;
constant etaReduceLayers    : naturals( numEtaReduceLayers - 1 downto 0 ) := ( 5, 12 ); -- decrease minStubLayers by 1 in these eta subSectors

constant numHTblocks: integer := 2;

constant numBlocksGPHT: natural := 4;
constant widthHexDTC   : natural := width( DTCsPerRegion / 6 );
constant widthHexModule: natural := width( 6 * modulesPerDTC );
constant widthCorPhi  : natural := 2;
constant widthCorEta  : natural := 2;

--
-- DERIVATIVES
--

constant invPtToDphi: real := bField * speedOfLight / 2.0E3;                          -- converts 1/2R in qOverPt (is actually a negative number)
constant maxCot: real := sinh( maxEta );

constant region: natural := dtcID / DTCsPerRegion;

constant numLinksDTC: natural := TMPtfp * numOverlap;
constant CICsPerDTC: natural := modulesPerDTC * numCIC;

constant numMBins: integer := houghNbinsPt / 2;
constant numCBins: integer := houghNbinsPhi / 2;

constant numLinksHT:  natural := numHTblocks * numMbins / 2; -- number of links between HT and KF
constant numLinksGP:  natural := 2 * DTCsPerRegion;          -- number of links between DTC and GP

constant neededRangeM  : real := 2.0 / real( houghMinPt ) * invPtToDphi;
constant neededRangeC  : real := 2.0 * MATH_PI / real( numRegions ) / real( numSectorsPhi );
constant neededRangeR  : real := trackerOuterRadius - trackerInnerRadius;
constant neededRangeZ  : real := 2.0 * trackerHalfLength;
constant neededRangePhi: real := 2.0 * MATH_PI / real( numRegions ) / real( numSectorsPhi ) + neededRangeM * neededRangeR / 4.0;
constant neededRangeCot: real := 2.0 * maxCot;

constant neededRangePhiDTC: real := 2.0 * MATH_PI / real( numRegions ) + neededRangeM * neededRangeR / 4.0;

constant numCotTiles        : natural := sum( multiplierCot );
constant baseCotTile        : real := neededRangeCot / real( numCotTiles );

constant baseM      : real    := neededRangeM / real( numMBins );
constant baseC      : real    := neededRangeC / real( numCBins );
constant baseDiffPhi: integer := widthPhi - width( neededRangePhi / baseC );
constant basePhi    : real    := baseC * 2.0 ** real( -baseDiffPhi );
--constant basePhi    : real    := MATH_2_PI / real( 18 * 64 );
constant baseDiffR  : integer := widthR - integer( ceil( log2( neededRangeR / baseC * baseM ) ) );
constant baseR      : real    := baseC / baseM * 2.0 ** real( -baseDiffR );

constant critR: real := floor( ( trackerInnerRadius + neededRangeR / 2.0 ) / baseR ) * baseR;

constant baseZTile          : real := baseCotTile * critR;
constant baseDiffZ  : integer := widthZ - integer( ceil( log2( neededRangeZ / baseZTile ) ) );
constant baseZ      : real    := baseZTile * 2.0 ** real( -baseDiffZ );

constant neededRangeChiZ  : real := real( max( multiplierCot ) ) * baseZTile + 4.0 * beamWindowZ * ( trackerOuterRadius / critR - 1.0 );
constant neededRangeChiPhi: real := 2.0 * baseC;

constant numSectors    : natural := numSectorsEta * numSectorsPhi;
constant widthBinsPt   : natural := width( houghNbinsPt );
constant widthBinsPhi  : natural := width( houghNbinsPhi );
constant widthChiZ     : natural := natural( ceil( log2( neededRangeChiZ / baseZ ) ) );
constant widthChiPhi   : natural := width( neededRangeChiPhi / basePhi );
constant widthModule   : natural := width( modulesPerDTC );
constant widthDTC      : natural := width( DTCsPerRegion );
constant widthSector   : natural := width( numSectors );
constant widthSectorPhi: natural := width( numSectorsPhi );
constant widthSectorEta: natural := width( numSectorsEta );
constant widthStubs    : natural := width( numStubs );
constant widthMBin  : integer := width( numMBins );
constant widthCBin  : integer := width( numCBins );

constant widthPhiDTC: natural := width( neededRangePhiDTC / basePhi );

constant baseBend       : real    := 2.0 ** baseDiffBend;
constant widthBendMinMax: natural := width( 2.0 ** widthBend - 1.0 + bendRes / baseBend );
constant widthDbend     : natural := width( bendRes / baseBend ) + 1;

function init_tileToEta return naturals;
constant tileToEta: naturals( numCotTiles - 1 downto 0 );

constant baseDiffPhiM: integer := widthRow - widthRowLUT;
constant basePhiM: real := basePhi * 2.0 ** ( -baseDiffPhiM );
constant maxPitch: real := 0.01;
constant x1: real := 2.0 ** widthRow * baseRow * maxPitch / 2.0;
constant x0: real := x1 - 2.0 ** widthRowLUT * baseRow * maxPitch;
constant maxPhiM: real := arctan( x1, trackerInnerRadius ) - arctan( x0, trackerInnerRadius );
constant widthPhiM: natural := width( maxPhiM / basePhiM );

constant widthMDTC: natural := 9;
constant baseDiffMDTC: integer := widthMDTC - widthMBin;
constant baseMDTC: real := baseM * 2.0 ** ( -baseDiffMDTC );

end;


package body config is


function init_tileToEta return naturals is
    variable e: naturals( numCotTiles - 1 downto 0 );
    variable k: integer := 0;
begin
    for iEta in 0 to numSectorsEta - 1 loop
        for iTile in 0 to multiplierCot( iEta ) - 1 loop
            e( k ) := iEta;
            k := k + 1;
        end loop;
    end loop;
    return e;
end function;
constant tileToEta: naturals( numCotTiles - 1 downto 0 ) := init_tileToEta;

end;
