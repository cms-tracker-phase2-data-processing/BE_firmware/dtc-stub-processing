library ieee, std;
use std.textio.all;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use work.tools.all;


package trackerGeometry is


constant dtcId: natural := 139;

constant numRegions         : natural := 9;      -- nononants or octants or etc
constant modulesPerDTC      : natural := 72; -- max number of modules per DTC
constant DTCsPerRegion      : natural := 24; -- max number of DTC per Nonant

constant halfDTCsPerRegion: natural := DTCsPerRegion / 2;

constant psModule: boolean := dtcId mod halfDTCsPerRegion < ( halfDTCsPerRegion / 2 );

constant numLayers     : natural := 7;     -- max number of detector layers/discs a track may cross
constant widthLayer    : natural := work.tools.width( numLayers );

function tkId( my: integer ) return integer;
function myId( tk: integer ) return integer;

constant widthTXT: natural := 1 + widthLayer + 7 * widthDouble;
type t_moduleTXT is
record
    valid:    std_logic;
    layer:    std_logic_vector( widthLayer - 1 downto 0 );
    sep:      std_logic_vector( widthDouble - 1 downto 0 );
    tilt:     std_logic_vector( widthDouble - 1 downto 0 );
    pitchCol: std_logic_vector( widthDouble - 1 downto 0 );
    pitchRow: std_logic_vector( widthDouble - 1 downto 0 );
    r:        std_logic_vector( widthDouble - 1 downto 0 );
    phi:      std_logic_vector( widthDouble - 1 downto 0 );
    z:        std_logic_vector( widthDouble - 1 downto 0 );
end record;
function conv( txt: bit_vector ) return t_moduleTXT;

type t_module is
record
    valid:    boolean;
    layer:    natural;
    r:        real;
    phi:      real;
    z:        real;
    sin:      real;
    cos:      real;
    sep:      real;
    pitchCol: real;
    pitchRow: real;
end record;
type t_modules is array ( 0 to modulesPerDTC - 1 ) of t_module;

impure function init_modules return t_modules;
constant modules: t_modules;


end;



package body trackerGeometry is


function tkId( my: integer ) return integer is
    variable tk, side, region, slot: integer;
begin
    region := my / DTCsPerRegion;
    slot := my mod halfDTCsPerRegion;
    side := ( my mod DTCsPerRegion ) / halfDTCsPerRegion;
    tk := ( side * numRegions + region ) * halfDTCsPerRegion + slot + 1;
    return tk;
end function;

function myId( tk: integer ) return integer is
    variable my, side, region, slot: integer;
begin
    my := tk - 1;
    side := my / ( numRegions * halfDTCsPerRegion );
    region := ( my mod ( numRegions * halfDTCsPerRegion ) ) / halfDTCsPerRegion;
    slot := my mod halfDTCsPerRegion;
    my := region * DTCsPerRegion + side * halfDTCsPerRegion + slot;
    return my;
end function;

function conv( txt: bit_vector ) return t_moduleTXT is
    variable t: t_moduleTXT;
    variable s: std_logic_vector( widthTXT - 1 downto 0 ) := to_stdlogicvector( txt );
begin
    t.valid    := s( widthTXT - 1 );
    t.layer    := s( widthTXT - 1    - 1 downto widthTXT - widthLayer - 1 );
    t.sep      := s( 7 * widthDouble - 1 downto 6 * widthDouble );
    t.tilt     := s( 6 * widthDouble - 1 downto 5 * widthDouble );
    t.pitchCol := s( 5 * widthDouble - 1 downto 4 * widthDouble );
    t.pitchRow := s( 4 * widthDouble - 1 downto 3 * widthDouble );
    t.r        := s( 3 * widthDouble - 1 downto 2 * widthDouble );
    t.phi      := s( 2 * widthDouble - 1 downto 1 * widthDouble );
    t.z        := s( 1 * widthDouble - 1 downto 0 * widthDouble );
    return t;
end function;

impure function init_modules return t_modules is
    file f: text open read_mode is "../../cfg/luts/dtc_" & integer'image( dtcId ) & ".txt";
    variable l: line;
    variable w: bit_vector( widthTXT - 1 downto 0 );
    variable modules: t_modules := ( others => ( false, 0, others => 0.0 ) );
    variable m: t_module;
    variable t: t_moduleTXT;
    variable tilt: real;
begin
    for k in modules'range loop
        readline( f, l );
        read( l, w );
        t := conv( w );
        if t.valid = '1' then
            m.valid := true;
            m.layer := uint( t.layer );
            m.r := dreal( t.r );
            m.phi := dreal( t.phi );
            m.z := dreal( t.z );
            tilt := dreal( t.tilt );
            m.sin := sin( tilt );
            m.cos := cos( tilt );
            m.sep := dreal( t.sep );
            m.pitchCol := dreal( t.pitchCol );
            m.pitchRow := dreal( t.pitchRow );
            modules( k ) := m;
        end if;
    end loop;
    return modules;
end function;

constant modules: t_modules := init_modules;

end;
