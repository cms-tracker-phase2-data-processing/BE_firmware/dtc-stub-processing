library ieee;
use ieee.std_logic_1164.all;
use work.emp_device_decl.all;
use work.emp_data_types.all;
use work.config.all;
use work.dtc_stubs.all;
use work.dtc_config.all;
use work.dtc_link_maps.all;


entity StubProcessor is
port (
    --- Input Ports ---
    clk_p      : in std_logic;
    dtc_din  : in ldata(cNumberOfFEModules - 1 downto 0);
    --- Output Ports ---
    dtc_dout : out ldata(cNumberOfOutputLinks - 1 downto 0)
);
end;


architecture rtl of StubProcessor is

    signal transform_dout    : t_stubsTransform(cNumberOfFEModules - 1 downto 0) := (others => nullStub);
    signal route_din         : t_stubsTransform(cNumberOfFEModules - 1 downto 0) := (others => nullStub);
    signal route_dout        : t_stubsRoute(routeStubs - 1 downto 0)             := (others => nullStub);
    signal mux_din           : t_stubsRoute(routeStubs - 1 downto 0)             := (others => nullStub);
    signal mux_dout          : t_stubsDTC(cNumberOfOutputLinks - 1 downto 0)     := (others => nullStub);
    signal formatOutput_din  : t_stubsDTC(cNumberOfOutputLinks - 1 downto 0)     := (others => nullStub);
    signal formatOutput_dout : ldata(cNumberOfOutputLinks - 1 downto 0)          := (others => ( ( others => '0' ), '0', '0', '0', '1', '0' ));

begin

    --==============================--
    -- Formatting Stage
    --==============================--

    genFormatters : for i in cNumberOfFEModules - 1 downto 0 generate
        signal formatInput_dout : t_stubFE := nullStub;
        signal transform_ipbus  : t_ipbus  := nullBus;
        signal transform_din    : t_stubFE := nullStub;
    begin
        transform_din <= formatInput_dout;

        --==============================--
        FormatInputNodeInstance :  entity work.dtc_formatInput_node
        --==============================--
        port map (
            --- Input Ports ---
            clk       => clk_p,
            node_din  => dtc_din(i),
            --- Output Ports ---
            node_dout => formatInput_dout
        );

        --==============================--
        TransformNodeInstance : entity work.dtc_transform_node
        --==============================--
        port map (
            --- Input Ports ---
            clk        => clk_p,
            node_din   => transform_din,
            --- Input Ports ---
            node_dout  => transform_dout(i),
            --- IPBus Ports ---
            node_ipbus => transform_ipbus
        );

    end generate;


    --==============================--
    -- Routing Stage
    --==============================--

    route_din <= transform_dout;
    mux_din   <= route_dout;

    --==============================--
    RouteInstance : entity work.dtc_route
    --==============================--
    port map (
        --- Input Ports ---
        clk        => clk_p,
        route_din  => route_din,
        --- Output Ports ---
        route_dout => route_dout
    );

    --==============================--
    MuxInstance : entity work.dtc_mux
    --==============================--
    port map (
        --- Input Ports ---
        clk      => clk_p,
        mux_din  => mux_din,
        --- Output Ports ---
        mux_dout => mux_dout
    );


    --==============================--
    -- Output Formatting Stage
    --==============================--

    formatOutput_din <= mux_dout;
    dtc_dout         <= formatOutput_dout;

    genFormatOutputNodes : for i in cNumberOfOutputLinks - 1 downto 0 generate
    begin
        --==============================--
        FormatOutputNodeInstance : entity work.dtc_formatOutput_node
        --==============================--
        port map (
            --- Input Ports ---
            clk       => clk_p,
            node_din  => formatOutput_din(i),
            --- Output Ports ---
            node_dout => formatOutput_dout(i)
        );
    end generate;

end;
